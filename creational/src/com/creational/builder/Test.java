package com.creational.builder;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Computer comp = new Computer.ComputerBuilder(
				"500 GB", "2 GB").setBluetoothEnabled(true)
				.setGraphicsCardEnabled(false).build();
		System.out.println(comp.isGraphicsCardEnabled());
	}

}
	

/**
 * 	java.lang.StringBuilder#append() (unsynchronized)
	java.lang.StringBuffer#append() (synchronized)
 */
