package com.creational.builder;

public class Computer {

	// required parameters
	private String HDD;
	private String RAM;
	
	// optional params
	private boolean isGraphicsCardEnabled;
	private boolean isBluetoothEnabled;

	public String getHDD() {
		return HDD;
	}

	
	public String getRAM() {
		return RAM;
	}

	public boolean isBluetoothEnabled() {
		return isBluetoothEnabled;
	}


	public boolean isGraphicsCardEnabled() {
		return isGraphicsCardEnabled;
	}


	/**
	 * @param hDD
	 * @param rAM
	 * @param isBluetoothEnabled
	 */
	private Computer(ComputerBuilder builder) {
		HDD = builder.HDD;
		RAM = builder.RAM;
		this.isBluetoothEnabled = builder.isBluetoothEnabled;
		this.isGraphicsCardEnabled=builder.isGraphicsCardEnabled;
	}

	// builder class
	
	public static class ComputerBuilder
	{
		// required parameters
				private String HDD;
				private String RAM;

				// optional parameters
				private boolean isGraphicsCardEnabled;
				private boolean isBluetoothEnabled;
				
				public ComputerBuilder(String hdd, String ram){
					this.HDD=hdd;
					this.RAM=ram;
				}
				
				public ComputerBuilder setBluetoothEnabled(boolean value)
				{
					this.isBluetoothEnabled = value;
					return this;
				}
				public ComputerBuilder setGraphicsCardEnabled(boolean value)
				{
					isGraphicsCardEnabled = value;
					return this;
				}
				
				public Computer build()
				{
					return new Computer(this);
					
				}
	}
}
