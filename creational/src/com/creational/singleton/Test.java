package com.creational.singleton;

import java.io.IOException;
import java.util.Hashtable;


class parent extends Thread implements Runnable
{
	public child m1(){System.out.println("parent");return null;}
	
}

class child extends parent
{
	//public void m1(){}
	
	public parent m1(){System.out.println("child");return null;}
}


public class Test {

	static String str ;
	
	static
	{
		String str="hi";
		str="XYZ";
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	private int eid=10;
	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub

		parent p = new parent();
		p.wait();
		EnumSingleton instance = EnumSingleton.INSTANCE;
		instance.doSomething();
		EnumSingleton instance1 = EnumSingleton.INSTANCE;
		System.out.println(instance.hashCode());
		System.out.println(instance1.hashCode());
		
		StringBuffer sb = new StringBuffer("a");
		sb.append("1234567891234567");
		System.out.println(sb.length());
		System.out.println(sb.capacity());
		String [] res = {"ME","AB"};
		System.out.println(res[0]);
		Test t= new Test();
		System.out.println(t.check());
		
		t.dispy(null);
		//t.eid=eid;
		//t.clone();
		String str="ABC";
		System.out.println(Test.str);
		
		//Hashtable<String, String=""> h = new Hashtable<String, String="">();
	}
	
	void dispy(Object obj){}
	void dispy(String s){System.out.println("gg");}
	
	int check()
	{
		try
		{
			throw new IOException();
			//System.out.println("try");
	//		return 1;
		}
		catch(Exception e)
		{
			System.out.println("catch");
			return 2;
		}
		finally
		{
			//System.exit(0);
			System.out.println("final");
			return 3;
		}
	}
	
}
