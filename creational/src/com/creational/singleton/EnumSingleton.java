package com.creational.singleton;

public enum EnumSingleton implements Cloneable{

	INSTANCE;
	
	public void doSomething()
	{
		System.out.println("Enum Singleton");
	}
	
	
}
